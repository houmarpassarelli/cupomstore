import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule} from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { FormsModule, Validators } from '@angular/forms';

import { MyApp } from './app.component';
import { Ofertas } from '../pages/ofertas/ofertas';
import { Login } from '../pages/login/login';
import { Oferta } from '../pages/oferta/oferta';
import { Perfil } from '../pages/perfil/perfil';
import { Pacotes} from '../pages/pacotes/pacotes';
import { Favoritos } from '../pages/favoritos/favoritos';
import { Lista } from '../pages/lista/lista';
import { Filtros } from '../pages/filtro/filtro';
import { Cadastro, TermosECondicoes } from '../pages/cadastro/cadastro';
import { Parceiros } from '../pages/parceiros/parceiros';
import { MinhasOfertas, MinhasOfertasModalComment, MinhasOfertasModalQRCode } from '../pages/minhasofertas/minhasofertas';
import { Mercado } from '../pages/mercado/mercado';
import { Contatos } from '../pages/contatos/contatos';
import { cSlides} from '../pages/slides/slides';
import { Configuracoes } from '../pages/configuracoes/configuracoes';
import { ErrorFake } from '../pages/fake/errorfake';
import { Update } from '../pages/update/update';

const cloudSettings: CloudSettings = {
 'core': {
   'app_id': '6d9cdbcf'
 }
};

@NgModule({
  declarations: [
    MyApp,
    Ofertas,
    Login,
    Oferta,
    Perfil,
    Pacotes,
    Favoritos,
    Lista,
    Filtros,    
    Cadastro,
    Parceiros,
    MinhasOfertas,
    Mercado,
    Contatos,
    cSlides,
    Configuracoes,
    ErrorFake,
    MinhasOfertasModalComment,
    MinhasOfertasModalQRCode,
    Update,
    TermosECondicoes
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    CloudModule.forRoot(cloudSettings)
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Ofertas,
    Login,
    Oferta,
    Perfil,
    Pacotes,
    Favoritos,
    Lista,
    Filtros,    
    Cadastro,
    Parceiros,
    MinhasOfertas,
    Mercado,
    Contatos,
    cSlides,
    Configuracoes,
    ErrorFake,
    MinhasOfertasModalComment,
    MinhasOfertasModalQRCode,
    Update,
    TermosECondicoes
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ScreenOrientation,
    CloudModule,
    FormsModule,
    Validators
  ]
})
export class AppModule {}
