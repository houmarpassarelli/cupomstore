import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Storage } from '@ionic/storage';
import { Deploy } from '@ionic/cloud-angular';

import { Ofertas } from '../pages/ofertas/ofertas';
import { Login } from '../pages/login/login';
import { Perfil } from '../pages/perfil/perfil';
import { Pacotes} from '../pages/pacotes/pacotes';
import { Favoritos } from '../pages/favoritos/favoritos';
import { MinhasOfertas } from '../pages/minhasofertas/minhasofertas';
import { Mercado } from '../pages/mercado/mercado';
import { Contatos } from '../pages/contatos/contatos';
import { Configuracoes } from '../pages/configuracoes/configuracoes';
import { ErrorFake } from '../pages/fake/errorfake';
import { Update } from '../pages/update/update';

@Component({
  templateUrl : 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage : any;
  paginas : Array<{titulo : string, icone : string ,componente : any}>;
  usuariologado : string;
  usuarioimagem : string;
  usuarioemail : string;

  constructor(public modalCtrl : ModalController, 
              private storage : Storage, 
              platform : Platform, 
              statusBar: StatusBar, 
              splashScreen: SplashScreen, 
              private screenOrientation : ScreenOrientation,
              public deploy : Deploy) {

    this.paginas = [
      {titulo : "Meus Cupons", icone : "ios-pricetags-outline", componente : MinhasOfertas},
      {titulo : "Meus Favoritos", icone : "ios-heart-outline", componente : Favoritos},
      {titulo : "Pacotes", icone : "ios-cube-outline", componente : Pacotes},
      {titulo : "Troca de Cupons", icone : "ios-swap-outline", componente : Mercado},
      {titulo : "Contatos", icone : "ios-contacts-outline", componente : Contatos},
      {titulo : "Configurações", icone : "switch", componente : Configuracoes},
      {titulo : "Sair", icone : "ios-exit-outline", componente : 'exit'}
    ];

    platform.ready().then(() => {

      statusBar.styleDefault();
      splashScreen.hide();

    });

    //this.deploy.channel = 'production';
  }

  abrePagina(index) : void {

      if(index.componente == MinhasOfertas){
          this.nav.setRoot(index.componente);
      }
      else if(index.componente == 'exit'){
        this.storage.clear();
        this.nav.setRoot(Login);
      }
      else{
        this.nav.push(index.componente);
      }
  }

  abrePerfil() : void {
    this.nav.push(Perfil);
  }

  presentModal() {
    let modal = this.modalCtrl.create(Pacotes);
    modal.present();
  }

  ngAfterViewInit() {
    
    this.storage.get('usuariologado').then((dados) => {
        if(dados == null){
          this.rootPage = Login;
        }
        else{

          this.usuariologado = dados.nome;
          this.usuarioemail = dados.email;
          this.usuarioimagem = 'assets/images/user-avatar.png';

          // this.deploy.check().then((snapshotAvailable: boolean) => {
          //   if(snapshotAvailable){
          //     this.rootPage = Update;
          //   }
          //   else{
              this.rootPage = Pacotes;
          //   }
          // });  
        }
    });
  }

  getUserInfo() : void{

  }
}
