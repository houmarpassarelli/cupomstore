import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavController, LoadingController, MenuController} from 'ionic-angular';

@Component({
  selector : 'page-parceiros',
  templateUrl : 'parceiros.html'
})
export class Parceiros{

  resultado : Array<any>;  

  constructor(public navCtrl : NavController, public loadingCtrl : LoadingController, public menu : MenuController, private http : Http){
    //this.getParceiros();
    menu.enable(true);
  }

  getParceiros() : void{
    let loading = this.loadingCtrl.create({
        content : 'Carregando parceiros...'
    });

    loading.present();

    this.http.get('http://10.0.3.32:8080/cs_api/22935d7f7084b1ef474f7a006d2102ce/183628fa96d1c1ec88e09ebe83fb96b4').subscribe(response => {
      
      this.resultado = response.json();

      loading.dismiss();

    });
  }
}
