import { Component} from '@angular/core';
import { Http } from '@angular/http';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { Cadastro } from '../cadastro/cadastro';
import { Storage } from '@ionic/storage';
import { Pacotes } from '../pacotes/pacotes';


@Component({
  selector : 'page-login',
  templateUrl : 'login.html'
})
export class Login{

  login = {};
  usuariologado : any = {};

  constructor(private navCtrl : NavController, private alertaCtrl : AlertController, private storage : Storage, private http : Http, private loadingCtrl : LoadingController){}
  
  verificaLogin() : void{

    let loading = this.loadingCtrl.create({
        content : 'Realizando acesso...'
    });

    loading.present();

    this.http.post('http://10.0.3.32:8080/cs_api/d56b699830e77ba53855679cb1d252da/c0ef4307f8ff9b9beded5c3967c53e9a', this.login).subscribe(response => {

      loading.dismiss();

      let resposta = response.json();

      if(resposta.codigo == '200'){
        
        this.usuariologado = {
                                "id" : resposta.profile.codigo,
                                "nome" : resposta.profile.nome,
                                "sobrenome" : resposta.profile.sobrenome,
                                "avatar" : resposta.profile.avatar,
                                "email" : resposta.profile.email,
                                "endereco" : {
                                                "endereco" : resposta.profile.logradouro,
                                                "numero" : resposta.profile.numero,
                                                "bairro" : resposta.profile.bairro,
                                                "cep" : resposta.profile.cep,
                                                "cidade" : resposta.profile.cidade,
                                                "estado" : resposta.profile.estado
                                            }
                            };

        this.storage.set('usuariologado', this.usuariologado);

        this.navCtrl.setRoot(Pacotes);
      }
      else if(resposta.codigo == '0102'){
        this.showMessage('Usuário digitado inválido!');
      }
      else if(resposta.codigo == '0103'){
        this.showMessage('Senha digitada inválida!');
      }
    });
  }

  showMessage(texto : string) : void {
    let alerta = this.alertaCtrl.create({
      title: 'Acesso Inválido!',
      subTitle: texto,
      buttons: ['Voltar']
    });

    alerta.present();
  }

  novoCadastro() : void{
    this.navCtrl.push(Cadastro);
  }
}
