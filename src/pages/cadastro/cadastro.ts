import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavController, AlertController, LoadingController, ViewController, ModalController} from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Login } from '../login/login';

@Component({
  selector : 'page-cadastro',
  templateUrl : 'cadastro.html'
})
export class Cadastro{

  private register : FormGroup;

  constructor(private navCtrl : NavController, 
              private alertaCtrl : AlertController,
              private http : Http, 
              private loadingCtrl : LoadingController,
              private formBuilder : FormBuilder,
              private modalCtrl : ModalController
            ){

              this.register = this.formBuilder.group({
                nome : ['', Validators.required],
                sobrenome : ['', Validators.required],
                login : ['', Validators.required],
                email : ['', Validators.required],
                senha : ['', Validators.required],
                repeat : ['', Validators.required],
                agree : [false]
              });
            }

  registerForm(){
    
    let loading = this.loadingCtrl.create({
      spinner : 'dots',
      content : 'Aguarde, relizando seu cadastro...'
    });

    if(this.register.valid){
      if(this.register.value.senha == this.register.value.repeat){

        if(this.register.value.agree){

          loading.present();

          this.http.post('http://10.0.3.32:8080/cs_api/f8032d5cae3de20fcec887f395ec9a6a/', this.register.value).subscribe(response => {

            let resposta = response.json();

            loading.dismiss();

            switch (resposta.codigo) {
              case '200': this.alerta('Concluído!', 'Cadastro realizado com sucesso!', 'Realizar acesso', 'login'); break;
              case '0101': this.alerta('Usuário indisponível', 'Esse usuário já esta sendo utilizado', 'Voltar', 'fechar'); break;
              case '0201': this.alerta('Email inválido!', 'Digite um email válido', 'Voltar', 'fechar'); break;
              case '0202': this.alerta('Email indisponível', 'Esse email já esta sendo utilizado', 'Voltar', 'fechar'); break;
              default: this.alerta('Não realizado!', 'Não foi possível realizar seu cadastro. Entre em contato com o suporte e informe o código: ' + resposta.codigo, 'Voltar', 'fechar'); break;
            }

          }, (error) => {
            loading.dismiss();
            this.alerta('Não realizado!', 'Não foi possível realizar seu cadastro. Verifique sua conexão com a internet, se o problema persistir, entre em contato com o suporte', 'Voltar', 'fechar');            
          });
        }
        else{
          this.alerta('Ação necessária!','Aceite os Termos e Condições de Uso para realizar seu cadastro','Voltar','fechar');
        }             
      }
      else{
        this.alerta('Senha inválida!','Repita sua senha corretamente!','Voltar', 'fechar');
      }
    }
    else{
      this.alerta('Campos inválidos!', 'Preencha todos os campos para realizar o cadastro','OK', 'fechar');
    }
  }

  voltarAcesso() : void{
    this.navCtrl.setRoot(Login);
  }

  alerta(title : string, subTitle : string, btnTitle : string, retorno : string) : void{
    let alerta = this.alertaCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: [{
            text : btnTitle,
            handler : () => {
              if(retorno == 'login'){
                this.navCtrl.setRoot(Login);
              }
              else if(retorno == 'fechar'){
                return true;
              }
            }
        }]
      });

      alerta.present();
  }
  
  termoscondicoes() : void{
    this.modalCtrl.create(TermosECondicoes).present();
  }
}

@Component({
  selector : 'modal-termosecondicoes',
  template : `<ion-header>
              <ion-toolbar>
                <ion-title>Termos e Condições de Uso</ion-title>
                <ion-buttons end>
                  <button ion-button icon-only color="royal" (click)="close()">
                    <ion-icon name="close"></ion-icon>
                  </button>
                </ion-buttons>
              </ion-toolbar>
              </ion-header>
              <ion-content center text-center padding>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut justo quis felis sagittis auctor vitae ac metus. 
                  Phasellus tincidunt mauris fringilla pretium pellentesque. Nunc a vehicula neque, ultrices pretium sapien. 
                  Mauris varius volutpat felis dictum varius. Integer volutpat, arcu nec fringilla finibus, quam massa vulputate elit, quis maximus lorem tortor et felis. 
                  Curabitur eleifend laoreet turpis, sit amet condimentum elit cursus non. Donec eget ornare augue. Sed a ex vel nulla interdum suscipit. Donec sit amet gravida sem, eu venenatis felis. 
                  Proin ut posuere augue, nec pellentesque nunc. Fusce iaculis iaculis nisi, sit amet sagittis nisi finibus cursus.
                </p>
              </ion-content>`
})
export class TermosECondicoes {
  
  constructor(private viewCtrl : ViewController){}

  close() : void{
    this.viewCtrl.dismiss(this);
  }
}
