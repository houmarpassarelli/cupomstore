import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';

@Component({
  selector : 'page-favoritos',
  templateUrl : 'favoritos.html'
})
export class Favoritos{

  public resultado : Array<any>;

  constructor(public navCtrl : NavController, public loadingCtrl : LoadingController,){
      this.getFavoritos();
  }

  getFavoritos() : void {
    let loading = this.loadingCtrl.create({
        content : 'Carregando ofertas...'
    });

    loading.present();

    this.resultado = [
      {
      "ID" : "1",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-1.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-1.jpg",
      "FORNECEDOR_TITULO" : "Bem Goiano"
      },{
      "ID" : "2",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-2.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-2.jpg",
      "FORNECEDOR_TITULO" : "Galícia Restaurante"
      },{
      "ID" : "3",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-3.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-3.jpg",
      "FORNECEDOR_TITULO" : "Amaranto Restaurante"
      },{
      "ID" : "4",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-4.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-4.jpg",
      "FORNECEDOR_TITULO" : "Japinha Temakeria"
      },{
      "ID" : "5",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-5.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-5.jpg",
      "FORNECEDOR_TITULO" : "pizza&hungry"
      }
    ];

    loading.dismiss();
  }
}
