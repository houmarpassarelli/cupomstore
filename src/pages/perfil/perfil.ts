import { Component } from '@angular/core';
import { NavController, MenuController} from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector : 'page-perfil',
  templateUrl : 'perfil.html'
})
export class Perfil{

  usuariologado : string;
  usuarioimagem : string;
  usuarioemail : string;

  constructor(public navCtrl : NavController, public menu : MenuController, private storage : Storage){
      menu.enable(true);

      this.storage.get('usuariologado').then((login) => {
          switch(login){
            case 'houmar' :
              this.usuariologado = 'Houmar Passarelli';
              this.usuarioimagem = 'assets/images/houmar.jpg';
              this.usuarioemail = 'houmarpassarelli@gmail.com';
              break;
            case 'raphael' :
              this.usuariologado = 'Raphael Pires';
              this.usuarioimagem = 'assets/images/raphael.jpg';
              this.usuarioemail = 'raphapiress@gmail.com';
              break;
            case 'gleice' :
              this.usuariologado = 'Gleice Marques';
              this.usuarioimagem = 'assets/images/gleice.jpg';
              this.usuarioemail = 'gleicefmarques@gmail.com';
              break;
          }
      });
  }
}
