import { Component, Injectable } from '@angular/core';
import { NavController, LoadingController, MenuController, PopoverController, ModalController, ViewController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { Oferta } from '../oferta/oferta';
import { Pacotes} from '../pacotes/pacotes';
import { Filtros } from '../filtro/filtro';
import { cSlides } from '../slides/slides';

@Injectable()
export class MinhasOfertasRequest{
  
  constructor(public http : Http){}

  request(id : any, start : number) : Promise<any>{
    return new Promise(resolve =>{
      this.http.get('http://10.0.3.32:8080/cs_api/f61e7f04c539f2aa08d2ddb23be63f92/7d370f49aeaf1473b44755d8b0158003/' + id + '/3/'+ start).map(res => res.json()).subscribe(response =>{
        resolve(response);
      });
    });
  }
}

@Component({
  selector : 'page-minhasofertas',
  templateUrl : 'minhasofertas.html',
  providers : [MinhasOfertasRequest]
})
export class MinhasOfertas{

  public resultado : any = [];  
  public modal : any;
  public rating : any = false;
  //public comment : any = false;

  constructor(
    public navCtrl : NavController,
    public http : Http,
    public loadingCtrl : LoadingController,
    public menu : MenuController,
    public popCtrl : PopoverController,
    public modalCtrl : ModalController,
    private store : Storage,
    private requestOferta : MinhasOfertasRequest
  )
  {

    let loading = loadingCtrl.create({content : 'Aguarde, carregando suas ofertas...'});

    loading.present();

    this.store.get('usuariologado').then((resp)=>{
      this.loadOfertas(resp.id, 0).then(()=>{
        loading.dismiss();
      });
    });

    menu.enable(true);
  }

  loadOfertas(id : any, start : number) : any{

    return new Promise(resolve =>{
      this.requestOferta.request(id, start).then(data => {
        this.resultado.push(data);
        
        resolve(this.resultado);

      });
    });
  }

  doInfinite(infiniteScroll:any) : any{
      
      this.store.get('usuariologado').then((resp)=>{
        this.requestOferta.request(resp.id, this.resultado[0].length).then(data =>{
          
          for(let ofertas of data){
            this.resultado[0].push(ofertas);
          }

          infiniteScroll.complete();
        });
      });
  }

  abreOferta(id : any) : void{
    this.navCtrl.push(Oferta,{idOferta : id});
  }

  abrePacotes() : void {
    this.navCtrl.push(Pacotes);
  }

  presentPopover() : void {

    let popover = this.popCtrl.create(Filtros);

    popover.present();
  }

  abreQRCode(qrcode, altcode) : void {
    this.modalCtrl.create(MinhasOfertasModalQRCode, {"altcode": altcode, "qrcode" : qrcode}).present();
  }

  btnRating(codigo) : void{
    this.rating = codigo;
  }

  chooseRating(codigo : any, valor : any) : void{

    let dados = {
                  "usercode" : "",
                  "cupomcode" : codigo,
                  "rating" : valor
                };

    this.store.get('usuariologado').then(resp => {
      
      dados.usercode = resp.id;

      this.http.post('http://10.0.3.32:8080/cs_api/f61e7f04c539f2aa08d2ddb23be63f92/5ca6eb852234e7d4bf020273bdd11ce2', dados).subscribe(() => {
        this.rating = null;
      });
    });
  }

  comentario(id : any) : void{    
    this.modalCtrl.create(MinhasOfertasModalComment,{codigo : id}).present();
  }

  openSlider() : void{
    this.navCtrl.push(cSlides);
  }
}
@Component({
  selector : 'modal-minhasofertascomment',
  template : `<ion-header>
              <ion-toolbar>
                <ion-title>
                  Comentários
                </ion-title>
                <ion-buttons start>
                  <button ion-button (click)="close()">                    
                    <ion-icon name="md-close"></ion-icon>
                  </button>
                </ion-buttons>
              </ion-toolbar>
            </ion-header>
            <ion-content class="bkg-content">
              <div class="comment-place">
                <div class="folks-comment">
                  <ion-list *ngFor="let feed of comentarios">
                    <ion-item>
                      <ion-avatar item-start>
                        <img src="{{feed.img_usuario}}" style="display:inline-block;">
                      </ion-avatar>
                      <div style="display:inline-block;vertical-align: top;">
                        <h2>{{feed.nome}}</h2>
                        <p>{{feed.comentario}}</p>
                      </div>
                    </ion-item>
                  </ion-list>
                </div>
                <div class="own-comment">
                    <button ion-button (click)="addComentario()" clear><ion-icon name="chatbubbles"></ion-icon></button>
                    <ion-input type="text" [(ngModel)]="comment" placeholder="Escreva seu comentário..."></ion-input>
                </div>
            </div>
            </ion-content>`
  
})
export class MinhasOfertasModalComment{

  public comentarios : Array<any>;
  public comment : any = null;
  
  constructor(private viewCtrl : ViewController, private navParams : NavParams, private http : Http, private storage : Storage){
    this.loadComentario(this.navParams.get('codigo'));
  }

  close() : void {
    this.viewCtrl.dismiss(this);
  }

  addComentario(){
    
    let dados = {
                  "usercode" : "",
                  "cupomcode" : this.navParams.get('codigo'),
                  "comment" : this.comment,
                  "target" : 'oferta'
                }

    this.storage.get('usuariologado').then((resp) =>{
      dados.usercode = resp.id;
      this.http.post('http://10.0.3.32:8080/cs_api/f8032d5cae3de20fcec887f395ec9a6a/a6ebd6a69763c780b8cf5318d94136d2', dados).subscribe(() => {
        this.loadComentario(this.navParams.get('codigo'));
      });
    });
  }

  loadComentario(codigo : any){
    this.http.get('http://10.0.3.32:8080/cs_api/sf61e7f04c539f2aa08d2ddb23be63f92/7ca6176c074dcf2f6a774594d491634c/' + codigo).subscribe(response => {
        this.comentarios = response.json();
    });
  }
}
@Component({
  selector : 'modal-minhasofertasqrcode',
  template : `<ion-header>
              <ion-toolbar>
                <ion-title>Autenticação</ion-title>
                <ion-buttons end>
                  <button ion-button icon-only color="royal" (click)="close()">
                    <ion-icon name="close"></ion-icon>
                  </button>
                </ion-buttons>
              </ion-toolbar>
              </ion-header>
              <ion-content center text-center class="vertical-align-content bkg-content">
                <ion-grid>
                      <ion-row center>
                            <ion-col center>
                                <img src="{{qrcode}}" style="width:50%;display:block;margin:0 auto;"/>
                                <span>{{altcode}}</span>
                            </ion-col>
                      </ion-row>
                </ion-grid>
              </ion-content>`
})
export class MinhasOfertasModalQRCode{

  public altcode : any;
  public qrcode : any;

  constructor(private viewCtrl : ViewController, private navParams : NavParams){
    this.altcode = navParams.get('altcode');
    this.qrcode = navParams.get('qrcode');
  }

  close() : void {
    this.viewCtrl.dismiss(this);
  }

}
