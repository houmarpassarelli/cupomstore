import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  template : `
  <ion-list no-lines>
      <ion-list-header>Filtros</ion-list-header>
      <button ion-item>Filtro um</button>
      <button ion-item>Filtro dois</button>
      <button ion-item>Filtro três</button>
      <button ion-item>Filtro quatro</button>
  </ion-list>`
})
export class Filtros{

}
