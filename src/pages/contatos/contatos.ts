import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';

@Component({
    selector : 'page-contatos',
    templateUrl : 'contatos.html'
})
export class Contatos{

    public contacts : any;
    //public busca : boolean = false;

    constructor(){
        this.contacts = "my-contacts";
    }

    abreBusca(){
        //this.busca = !this.busca;
    }
}
