import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavController, MenuController , ViewController} from 'ionic-angular';

import { Parceiros } from '../parceiros/parceiros';
import { Ofertas } from '../ofertas/ofertas';

@Component({
  selector : 'page-pacotes',
  templateUrl : 'pacotes.html'
})
export class Pacotes{

  public pacotes : Array<any>;

  constructor(public navCtrl : NavController, public menu : MenuController, public viewCtrl : ViewController, private http : Http){
      
    menu.enable(true);

    this.http.get('http://10.0.3.32:8080/cs_api/c307f45ed96b6596975a477a22f199f0').subscribe(response => {
      this.pacotes = response.json();
    });

  }

  modalDismiss() : void{
    this.viewCtrl.dismiss(this);
  }

  getOfertas(id : any) : void{
    this.navCtrl.push(Ofertas,{
      idOfertas : id
    });
  }

  listaParceiros() : void{
    this.navCtrl.setRoot(Parceiros);
  }
}
