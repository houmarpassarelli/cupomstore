import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, MenuController, PopoverController, ModalController, Slides, NavParams, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { Oferta } from '../oferta/oferta'; 
import { MinhasOfertas } from '../minhasofertas/minhasofertas';


@Component({
  selector : 'page-ofertas',
  templateUrl : 'ofertas.html'
})
export class Ofertas{

  @ViewChild(Slides) slides : Slides;

  public popFav : boolean;
  public resultado : Array<any>;  
  public modal : any;
  public idOfertas : any;  

  constructor(
    public navCtrl : NavController,
    public navParams : NavParams,
    public http : Http,
    public loadingCtrl : LoadingController,
    public menu : MenuController,
    public popCtrl : PopoverController,
    public modalCtrl : ModalController,
    public alertCtrl : AlertController,
    private store : Storage
  )
  {
      this.getOfertas(navParams.get('idOfertas'));
      menu.enable(true);
      this.popFav = false;
      
  }

  getOfertas(id : any) : void {
    let loading = this.loadingCtrl.create({
        content : 'Carregando ofertas...'
    });

    loading.present();

    this.http.get('http://10.0.3.32:8080/cs_api/f61e7f04c539f2aa08d2ddb23be63f92/910572e3f281348ce7807952e993fada/' + id).subscribe(response =>{
      this.resultado = response.json();
      loading.dismiss();
    });
  }

  abreOferta() : void{
    this.navCtrl.push(Oferta);
  }

  abrePacotes() : void {
    this.navCtrl.pop();
  }

  comprarPacote() : void {

    let dados = {
      "pacote_id" : this.navParams.get('idOfertas'),
      "usuario_id" : ""
    };
    
    this.store.get('usuariologado').then((resp) => {
      dados.usuario_id = resp.id;
    });

    let confirma = this.alertCtrl.create({
      title: 'Comfirma compra?',
      message: 'Você confirma a compra desse pacote?',
      buttons: [
        {
          text: 'Não, voltar'
        },
        {
          text: 'Confirmar',
          handler: () => {

            let loading = this.loadingCtrl.create({
              content: 'Estamos processando seu pedido, aguarde...'
            });

            loading.present();
            
            console.log(dados);
            

            this.http.post('http://10.0.3.32:8080/cs_api/c307f45ed96b6596975a477a22f199f0/a9e250a864dc3526e8b4b947685a781b', dados).subscribe(response => {

              let resposta = response.json();

              if(resposta.codigo == '200'){

                loading.dismiss().then(()=>{
                  this.alertCtrl.create({
                    title: 'Compra finalizada!', 
                    message : 'Sua compra foi realizada com sucesso. Você já pode aproveitar seus cupons.', 
                    buttons :[{
                      text: 'OK',
                      handler : () => { 
                        this.navCtrl.setRoot(MinhasOfertas);
                      }
                    }]
                  }).present();
                });
              }
              else if(resposta.codigo == '23000'){
                loading.dismiss();
                this.alertCtrl.create({
                  title : "Compra não realizada!",
                  message : "Só é possível manter um pacote de cupons por vez. Aguarde uma nova opotunidade para adquirir um novo pacote.",
                  buttons : ['OK']
                }).present();
              }
              else{
                loading.dismiss();
                this.alertCtrl.create({
                  title : "Oops!",
                  message : "Não foi possível realizar sua compra, por gentileza procure o suporte.",
                  buttons : ['OK']
                });
              }
            });
          }
        }
      ]
    });
    
    confirma.present();
  }
}
