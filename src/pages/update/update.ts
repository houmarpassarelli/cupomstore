import { Component } from '@angular/core';
import { Deploy } from '@ionic/cloud-angular';

@Component({
    selector : 'page-update',
    templateUrl : 'update.html'
})
export class Update{
    constructor(private deploy : Deploy){}

    getUpdate() : void{
        this.deploy.download().then(() => this.deploy.extract()).then(() => this.deploy.load());
    }

}