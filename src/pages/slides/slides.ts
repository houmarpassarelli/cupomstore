import { Component } from '@angular/core';
import { NavController, ViewController} from 'ionic-angular';

@Component({
  selector : 'page-slides',
  templateUrl : 'slides.html'
})
export class cSlides{

  constructor(public viewCtrl : ViewController){}

  modalDismiss() : void{
    this.viewCtrl.dismiss(this);
  }

}