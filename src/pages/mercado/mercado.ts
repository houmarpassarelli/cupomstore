import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { Http } from '@angular/http';

@Component({
  selector : 'page-mercado',
  templateUrl : 'mercado.html'
})
export class Mercado{

  private mercado : Array<any>;
  private pendente : Array<any>;
  
  offer : any;
  loading : any;
  
  constructor(public http : Http, public loadingCtrl : LoadingController){
      
      this.offer = "ads";

      this.loading = this.loadingCtrl.create({
        content : 'Carregando ofertas...'
      });

      this.loading.present();

      this.getOfertas();
  }

  getOfertas() : void {

    this.http.get('http://10.0.3.32:8080/cs_api/f61e7f04c539f2aa08d2ddb23be63f92/a086654648ab1cbc20560ee7165d87db').subscribe(response =>{      
      this.mercado = response.json();
      this.loading.dismiss();
    });
  }
}