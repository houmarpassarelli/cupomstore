import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';

@Component({
  selector : 'page-lista',
  templateUrl : 'lista.html'
})
export class Lista{

  public resultado : Array<any>;

  constructor(public navCtrl : NavController){
    this.resultado = [
      {
      "ID" : "1",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-1.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-1.jpg",
      "FORNECEDOR_TITULO" : "Bem Goiano"
      },{
      "ID" : "2",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-2.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-2.jpg",
      "FORNECEDOR_TITULO" : "Galícia Restaurante"
      },{
      "ID" : "3",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-3.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-3.jpg",
      "FORNECEDOR_TITULO" : "Amaranto Restaurante"
      },{
      "ID" : "4",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-4.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-4.jpg",
      "FORNECEDOR_TITULO" : "Japinha Temakeria"
      },{
      "ID" : "5",
      "Titulo" : "Compre um e ganhe outro!",
      "IMG" : "assets/images/cupons/comida-5.jpg",
      "FORNECEDOR" : "assets/images/fornecedores/fornecedor-5.jpg",
      "FORNECEDOR_TITULO" : "pizza&hungry"
      }
    ];
  }
}
