import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavController, LoadingController, NavParams, AlertController} from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector : 'page-oferta',
  templateUrl : 'oferta.html'
})
export class Oferta{

  loading : any;

  constructor(public navCtrl : NavController, 
              private loadingCtrl : LoadingController, 
              private storage : Storage, 
              private http : Http, 
              private navParams : NavParams, 
              private alertCtrl : AlertController
            ){
    this.loading = loadingCtrl.create({
      content : 'Carregando oferta...'
    });

    this.loading.present();
  }

  ngAfterContentInit(){
    this.loading.dismiss();
  }

  putMarket(){

    let dados = {
                  "cupomcode" : this.navParams.get('idOferta'),
                  "usercode" : ""
                };

    this.alertCtrl.create({
      title: 'Confirma?',
      message: 'Você deseja realmente ofertar esse cupom no mercado de troca?',
      buttons: [
        {
          text: 'Não, voltar'
        },{
          text : 'Sim, ofertar',
          handler : () =>{
                  this.storage.get('usuariologado').then((resp) => {
                  dados.usercode = resp.id;
                  
                  let loading = this.loadingCtrl.create({content: 'Aguarde, processando seu pedido...'});
                  loading.present();

                  this.http.post('http://10.0.3.32:8080/cs_api/f61e7f04c539f2aa08d2ddb23be63f92/dc122f8086185814ff083b7ce05509d4', dados).subscribe(response => {
                      
                    let resposta = response.json();
                    
                    loading.dismiss();

                    if(resposta.codigo == '200'){
                      
                      this.alertCtrl.create({
                        title : 'Sucesso!',
                        message : 'Seu cupom foi colocado no mercado de troca. Aguarde até os lances para o cupom ou então você pode retirar ele de oferta a qualquer momento.',
                        buttons : ['OK']
                      }).present();
                    }
                    else{
                        this.alertCtrl.create({
                        title : 'Oops!',
                        message : 'Seu cupom já se encontra em oferta no mercado de trocas!',
                        buttons : ['OK']
                      }).present();
                    }
                  });
                });
          }
        }
      ]
    }).present();
  }

}
